// Import express module
var express = require("express");
var bodyParser = require("body-parser");

// init express object
var app = express();
app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());


//app.get("/register", function(req , res){
app.post("/register", function(req , res){
    console.info(req);
    /*
    var username = req.query.username;
    var email = req.query.email;
    var gender = req.query.gender;*/

    var username = req.body.params.username;
    var email = req.body.params.email;
    var gender = req.body.params.gender;
    console.info("Server side  username : %s" , username);
    console.info("Server side email : %s" , email);
    console.info("Server side gender : %s" , gender);
    res.status(200).end();
});

app.get("/register2", function(req , res){
    var username = req.query.username;
    var email = req.query.email;
    var gender = req.query.gender;
    console.info("Server side  username : %s" , username);
    console.info("Server side email : %s" , email);
    console.info("Server side gender : %s" , gender);
    res.status(200).end();
});

app.get("/thankyou", function(req , res) {
    res.redirect("thankyou.html");
});

app.get("/showNames", function(req, res) {
    var data = [ {"firstname": "John", "lastname": "Smith", "DOB": new Date},
        {"firstname": "Anna", "lastname" : "Doe" , "DOB": new Date},
        {"firstname": "Peter", "lastname" : "Jones" , "DOB": new Date},
        {"firstname": "Kenneth", "lastname" : "Phang" , "DOB": new Date}];
    res.json(data);
});

app.use(express.static(__dirname + "/public"));

app.listen(3000, function(){
    console.info("Webserver started at 3000");
});