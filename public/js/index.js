/**
 * Created by phangty on 1/7/16.
 */
var RegApp = angular.module("RegApp", []);

(function(){
    var RegCtrl;

    RegCtrl = function($http, $window){
        var ctrl = this;
        ctrl.username = "";
        ctrl.email = "";
        ctrl.gender = "";

        ctrl.names = [];
        $http.get("/showNames").success(function(data){
            console.info(data);
            ctrl.names =  data;
        });

        console.info("Client Side (c) username : " + ctrl.username);
        console.info("Client Side (c)  email : " + ctrl.email);
        console.info("Client Side (c)  gender :" + ctrl.gender);
        ctrl.register = function() {
            $http.post("/register", {
                params: {
                    username: ctrl.username,
                    email: ctrl.email,
                    gender: ctrl.gender
                }
            }).then( function(){
               console.info("Success");
                //$window.location = "/thankyou";
            }).catch( function(){
                console.error("error");
            });
        };

    };

    RegApp.controller("RegCtrl", ['$http', '$window', RegCtrl]);

})();